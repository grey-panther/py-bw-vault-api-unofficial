import os
import unittest
import json
import lzma
import shutil
import tempfile

import vault_management_api_client
import vault_management_api_client.api.lock_unlock.post_unlock as post_unlock
import vault_management_api_client.api.vault_items.get_list_object_items as vault_items
import vault_management_api_client.bw_process_manager as bwpm
import vault_management_api_client.models.post_unlock_body as post_unlock_body


class E2eTest(unittest.TestCase):
  __BW_HOST = '127.7.1.5'
  __BW_PORT = 9091

  @classmethod
  def setUpClass(cls):
    cls.__TEMP_DIR = tempfile.mkdtemp()
    cls.__BW_BINARY = os.path.join(cls.__TEMP_DIR, 'bw')
    with lzma.open(os.path.join(os.path.dirname(os.path.realpath(__file__)), 'bw.xz'), 'rb') as fin:
      with open(cls.__BW_BINARY, 'wb') as fout:
        shutil.copyfileobj(fin, fout, 1024*1024)
    os.chmod(cls.__BW_BINARY, 0o500)

  @classmethod
  def tearDownClass(cls):
    shutil.rmtree(cls.__TEMP_DIR)

  def setUp(self):
    self.username = os.environ['TEST_USERNAME']
    self.password = os.environ['TEST_PASSWORD']
    self.bw_process_manager = bwpm.BwProcess(self.__BW_BINARY, extra_env_vars={'BITWARDENCLI_APPDATA_DIR': self.__TEMP_DIR})
    self.bw_process_manager.start_serve(self.password, self.__BW_HOST, self.__BW_PORT, username=self.username)
    self.bw_url = f'http://{self.__BW_HOST}:{self.__BW_PORT}/'

  def tearDown(self):
    self.bw_process_manager.terminate_serve()

  def test_end_to_end(self):
    client = vault_management_api_client.Client(self.bw_url)

    def _run(api_class, body=None):
      kwargs = {'client': client}
      if body:
        kwargs['body'] = body
      response = api_class.sync_detailed(**kwargs)
      self.assertEqual(response.status_code, 200)
      response = json.loads(response.content)
      self.assertTrue(response['success'])
      return response['data']

    response = _run(
      post_unlock, post_unlock_body.PostUnlockBody(password=self.password))
    self.assertEqual(response['title'], 'Your vault is now unlocked!')

    response = _run(vault_items)
    self.assertEqual(response['object'], 'list')
    self.assertEqual(len(response['data']), 1)
    self.assertEqual(response['data'][0]['login']['username'], 'TestUsername')


if __name__ == '__main__':
    unittest.main()

