import unittest
import tempfile
import shutil
import stat
import os
import subprocess

import vault_management_api_client.bw_process_manager as bwpm

class CliProcessManagerTest(unittest.TestCase):
  temp_dir : str = ''  # to make sure that mypy knows about this attribute

  @classmethod
  def setUpClass(cls):
    cls.temp_dir = tempfile.mkdtemp(prefix='bw_process_manager_test_')
    cls.dummy_script_path = cls._createScript('dummy', '')

  @classmethod
  def tearDownClass(cls):
    shutil.rmtree(cls.temp_dir)

  @classmethod
  def _createScript(cls, name: str, contents: str) -> str:
    path = os.path.join(cls.temp_dir, f'{name}.sh')
    if os.path.exists(path):
      raise ValueError('"%s" already exists!' % path)
    with open(path, 'w') as f:
      f.write('#!/bin/sh\n')
      f.write(contents)
    st = os.stat(path)
    os.chmod(path, st.st_mode | stat.S_IXUSR)
    return path

  def testCreateScriptWithExistingScriptRaisesException(self):
    self._createScript('dummy2', '')
    with self.assertRaises(ValueError):
      self._createScript('dummy2', '')

  def testPmValidatesCliExists(self):
    fake_path = os.path.join(self.temp_dir, 'does_not_exists')
    with self.assertRaises(ValueError):
      bwpm.BwProcess(fake_path)

  def testPmValidatesCliIsExecutable(self):
    fake_path = os.path.join(self.temp_dir, 'is_not_executable')
    with open(fake_path, 'wb'):
      pass
    with self.assertRaises(ValueError):
      bwpm.BwProcess(fake_path)

  def testRun(self):
    pm = bwpm.BwProcess('/usr/bin/echo')
    self.assertIsNone(pm.run('test'))

  def testRunWithTimeoutRaises(self):
    sleep_script = self._createScript('sleep', '/usr/bin/sleep 5')
    pm = bwpm.BwProcess(sleep_script)
    with self.assertRaises(subprocess.TimeoutExpired):
      self.assertIsNone(pm.run('', timeout=0.1))

  def testRunWithParse(self):
    echo_script = self._createScript('echo_value', '/usr/bin/echo 5')
    pm = bwpm.BwProcess(echo_script)
    self.assertEqual(5, pm.run('', parse_output=True))

  def testForStartServeClientFieldsMustNotBeSetIfUsernameIsSet(self):
    pm = bwpm.BwProcess(self.dummy_script_path)
    with self.assertRaises(ValueError):
      pm.start_serve('', '', 0, username="foo", clientid="foo")
    with self.assertRaises(ValueError):
      pm.start_serve('', '', 0, username="foo", clientsecret="foo")

  def testBothClientIdAndClientSecretNeedsToBeSet(self):
    pm = bwpm.BwProcess(self.dummy_script_path)
    with self.assertRaises(ValueError):
      pm.start_serve('', '', 0, clientid="foo")
    with self.assertRaises(ValueError):
      pm.start_serve('', '', 0, clientsecret="foo")


if __name__ == '__main__':
    unittest.main()

