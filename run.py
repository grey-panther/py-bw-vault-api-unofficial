#!/usr/bin/python3
import json
import glob
import os
import shlex
import shutil
import subprocess
import sys


def _msg(msg):
  print('[*] ' + msg)


def _run(*args):
  _msg(f'Running {shlex.join(args)}')
  subprocess.check_call(args)


def _ensure_dev_venv_exists():
  if os.path.isdir('venv-dev'):
    return
  _msg('Virtual env for dev environment doesn\'t exits, creating')
  _run('/usr/bin/python3', '-m', 'venv', 'venv-dev')
  _msg('Installing development packages')
  _run('./venv-dev/bin/pip', 'install', '-r', 'requirements-dev-frozen.txt')


def _regenerate():
  _ensure_dev_venv_exists()

  os.chdir('openapi-definition/')

  _msg('Formatting schema')
  with open('vault-management-api.json') as f:
    schema = json.load(f)
  with open('vault-management-api-formatted.json', 'w') as f:
    json.dump(schema, f, indent=2, sort_keys=True)

  shutil.copy('vault-management-api-formatted.json', 'vault-management-api-patched.json')
  patches = sorted(glob.glob('*.patch'))
  if not patches:
    raise ValueError('Patches not found!')
  for i, fn in enumerate(patches):
    _msg(f'Applying patch {i+1}/{len(patches)}: {fn}')
    _run('/usr/bin/patch', 'vault-management-api-patched.json', fn)

  os.chdir('..')

  _msg('Generating python client')
  os.environ['PATH'] += os.pathsep + os.path.realpath('venv-dev/bin')
  _run(
    'openapi-python-client', 'generate', '--overwrite', '--fail-on-warning',
    '--path', 'openapi-definition/vault-management-api-patched.json')
  _msg('Done')


def _update_dev_venv_deps():
  if os.path.isdir('venv-dev'):
    _msg('Deleting venv-dev')
    shutil.rmtree('venv-dev')
  _msg('Creating virtual env for dev environment')
  _run('/usr/bin/python3', '-m', 'venv', 'venv-dev')
  _msg('Installing development packages')
  _run('./venv-dev/bin/pip', 'install', '-Ur', 'requirements-dev.txt')
  _msg('Storing installed version of packages')
  packages = subprocess.check_output(['./venv-dev/bin/pip', 'freeze'])
  with open('requirements-dev-frozen.txt', 'wb') as f:
    f.write(packages)
  _msg('Done')


def _test():
  _ensure_dev_venv_exists()

  if not os.path.isdir('venv'):
    _msg('Creating test venv')
    _run('/usr/bin/python3', '-m', 'venv', 'venv')

  _msg('Installing package into venv')
  _run('./venv/bin/pip', 'install', 'vault-management-api-client/')
  _run('./venv-dev/bin/ruff', 'check', '.')
  _run('./venv-dev/bin/mypy', 'vault-management-api-client/', 'tests/')
  _run('./venv/bin/python3', '-m', 'unittest', 'discover', 'tests/', '--verbose')
  _msg('Done')


def _publish():
  _ensure_dev_venv_exists()
  shutil.copy('README.md', 'vault-management-api-client/README.md')
  shutil.copy('LICENSE', 'vault-management-api-client/LICENSE')
  os.chdir('vault-management-api-client')
  _run('../venv-dev/bin/poetry', 'publish', '--build')

commands = {
  'regenerate': _regenerate,
  'update-dev-env-dependencies': _update_dev_venv_deps,
  'test': _test,
  'publish': _publish,
}
if len(sys.argv) != 2:
  raise ValueError('Need exactly one parameter')
command = commands.get(sys.argv[1])
if not command:
  raise ValueError(f'Command "{sys.argv[1]}" not found. Needs to be one of {list(commands.keys())}')
root_dir = os.path.dirname(os.path.realpath(__file__))
_msg(f'Changing working directory to {root_dir}')
os.chdir(root_dir)
command()

